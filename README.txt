-- SUMMARY --

This module allows the blocks to move between regions so
fast, without having to enter the path: structure / blocks, when you move block 
this module save automatically the order of the block. 
Thanks to jQuery and Jquery-Sortable.


To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/drag_drop_block


-- REQUIREMENTS --

None.


-- INSTALLATION --

You have to enable module Drag and Drop Block.


-- CONFIGURATION --

* Once installed, go to /admin/people/permissions
* Find "Access block drag and drop", and give permission for the roles.

-- CONTACT --

Current maintainers:
* Saul Ramos (sdramos) - http://drupal.org/user/24550
