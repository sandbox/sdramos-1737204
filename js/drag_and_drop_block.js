(function ($) {
  $(function() {
    $('.region').sortable({
      placeholder: "ui-state-highlight",
      connectWith: '.region',
      update: function(event, ui) {

        var id_block = ui.item[0].id;
        var attr = $(this).attr("class");
        var order = $(this).sortable("toArray").toString();
        var opt = 0;
        // When moving between regions.
        if (ui.sender){
          opt = 1;
        }
        // When moving within a region.
        else {
          region_global = attr;
          opt = 0;
        }
        $.ajax({
          beforeSend: function(objeto) { },
          dataType : "json",
          url: Drupal.settings.basePath + "drag-and-drop-block/ajax/update",
          type: "POST",
          data: "opt=" + opt + "&id_block=" + id_block + "&region_global=" + region_global + "&regions_classes=" + attr + "&order=" + order,
          success: function(response){ }
        });
      }
    });
  });
})(jQuery);
